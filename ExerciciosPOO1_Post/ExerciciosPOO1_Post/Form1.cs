﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExerciciosPOO1_Post
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Post meuPost = new Post();


        private void Titulo_TextChanged(object sender, EventArgs e)
        {
            
        }
        private void Descricao_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonGosto_Click(object sender, EventArgs e)
        {

            meuPost.ContarGosto();
            LabelGosto.Text = meuPost.Gosto.ToString();
        }

        private void LabelGosto_Click(object sender, EventArgs e)
        {
            
        }


        private void ButtonNaoGosto_Click(object sender, EventArgs e)
        {
            meuPost.ContarNaoGosto();
            LabelNaoGosto.Text = meuPost.NaoGosto.ToString();
        }


        private void LabelNaoGosto_Click(object sender, EventArgs e)
        {

        }

        private void Enviar_Click(object sender, EventArgs e)
        {
            meuPost.Gosto = 0;
            meuPost.NaoGosto = 0;

            LabelMensagemEnviada.Text = "Titulo:" + TextTitulo.Text + "\n\nDescrição:\n" + TextDescricao.Text;
            LabelGosto.Text = meuPost.Gosto.ToString();
            LabelNaoGosto.Text = meuPost.NaoGosto.ToString();         
            
            LabelData.Text = "Data: " + meuPost.Data.ToString().Substring(0,10);

            TextTitulo.Text = null;
            TextDescricao.Text = null;
        }

       

        private void LabelData_Click(object sender, EventArgs e)
        {

        }

       

    }



}
