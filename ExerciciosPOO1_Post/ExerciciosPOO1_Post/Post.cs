﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciciosPOO1_Post
{
    class Post
    {

        private string _titulo;

        private string _descricao;

        private DateTime _data;

        private int _gosto;

        private int _naogosto;


        public string Titulo {get; set;}

        public string Descricao { get; set; }

        public DateTime Data { get; set; }
           
        
        public int Gosto { get; set; }

        public int NaoGosto { get; set; }




        public Post() : this(null,null) { }

     

        public Post(string titulo, string descricao)
        {
            Titulo = titulo;
            Descricao = descricao;
            Data = DateTime.Today;

        }

        public Post (Post post) : this (post.Titulo, post.Descricao) { }

        
               
        

        public void ContarGosto ()
        {
            Gosto++;
        }

        public void ContarNaoGosto()
        {
            NaoGosto++;
        }

        


    }
}
