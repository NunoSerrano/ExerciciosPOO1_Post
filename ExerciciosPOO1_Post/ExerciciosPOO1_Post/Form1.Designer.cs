﻿namespace ExerciciosPOO1_Post
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextTitulo = new System.Windows.Forms.TextBox();
            this.TextDescricao = new System.Windows.Forms.TextBox();
            this.ButtonEnviar = new System.Windows.Forms.Button();
            this.ButtonGosto = new System.Windows.Forms.Button();
            this.ButtonNaoGosto = new System.Windows.Forms.Button();
            this.LabelGosto = new System.Windows.Forms.Label();
            this.LabelMensagemEnviada = new System.Windows.Forms.Label();
            this.LabelNaoGosto = new System.Windows.Forms.Label();
            this.LabelUltimoPost = new System.Windows.Forms.Label();
            this.LabelData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextTitulo
            // 
            this.TextTitulo.Location = new System.Drawing.Point(56, 60);
            this.TextTitulo.Name = "TextTitulo";
            this.TextTitulo.Size = new System.Drawing.Size(399, 20);
            this.TextTitulo.TabIndex = 0;
            this.TextTitulo.TextChanged += new System.EventHandler(this.Titulo_TextChanged);
            // 
            // TextDescricao
            // 
            this.TextDescricao.Location = new System.Drawing.Point(56, 104);
            this.TextDescricao.Multiline = true;
            this.TextDescricao.Name = "TextDescricao";
            this.TextDescricao.Size = new System.Drawing.Size(399, 71);
            this.TextDescricao.TabIndex = 1;
            this.TextDescricao.TextChanged += new System.EventHandler(this.Descricao_TextChanged);
            // 
            // ButtonEnviar
            // 
            this.ButtonEnviar.Location = new System.Drawing.Point(364, 197);
            this.ButtonEnviar.Name = "ButtonEnviar";
            this.ButtonEnviar.Size = new System.Drawing.Size(91, 23);
            this.ButtonEnviar.TabIndex = 2;
            this.ButtonEnviar.Text = "Enviar";
            this.ButtonEnviar.UseVisualStyleBackColor = true;
            this.ButtonEnviar.Click += new System.EventHandler(this.Enviar_Click);
            // 
            // ButtonGosto
            // 
            this.ButtonGosto.Location = new System.Drawing.Point(56, 372);
            this.ButtonGosto.Name = "ButtonGosto";
            this.ButtonGosto.Size = new System.Drawing.Size(75, 23);
            this.ButtonGosto.TabIndex = 3;
            this.ButtonGosto.Text = "GOSTO";
            this.ButtonGosto.UseVisualStyleBackColor = true;
            this.ButtonGosto.Click += new System.EventHandler(this.ButtonGosto_Click);
            // 
            // ButtonNaoGosto
            // 
            this.ButtonNaoGosto.Location = new System.Drawing.Point(380, 372);
            this.ButtonNaoGosto.Name = "ButtonNaoGosto";
            this.ButtonNaoGosto.Size = new System.Drawing.Size(75, 23);
            this.ButtonNaoGosto.TabIndex = 4;
            this.ButtonNaoGosto.Text = "NÃO GOSTO";
            this.ButtonNaoGosto.UseVisualStyleBackColor = true;
            this.ButtonNaoGosto.Click += new System.EventHandler(this.ButtonNaoGosto_Click);
            // 
            // LabelGosto
            // 
            this.LabelGosto.AutoSize = true;
            this.LabelGosto.Location = new System.Drawing.Point(118, 414);
            this.LabelGosto.Name = "LabelGosto";
            this.LabelGosto.Size = new System.Drawing.Size(13, 13);
            this.LabelGosto.TabIndex = 5;
            this.LabelGosto.Text = "0";
            this.LabelGosto.Click += new System.EventHandler(this.LabelGosto_Click);
            // 
            // LabelMensagemEnviada
            // 
            this.LabelMensagemEnviada.AutoSize = true;
            this.LabelMensagemEnviada.Location = new System.Drawing.Point(53, 270);
            this.LabelMensagemEnviada.Name = "LabelMensagemEnviada";
            this.LabelMensagemEnviada.Size = new System.Drawing.Size(0, 13);
            this.LabelMensagemEnviada.TabIndex = 6;
            // 
            // LabelNaoGosto
            // 
            this.LabelNaoGosto.AutoSize = true;
            this.LabelNaoGosto.Location = new System.Drawing.Point(442, 414);
            this.LabelNaoGosto.Name = "LabelNaoGosto";
            this.LabelNaoGosto.Size = new System.Drawing.Size(13, 13);
            this.LabelNaoGosto.TabIndex = 9;
            this.LabelNaoGosto.Text = "0";
            this.LabelNaoGosto.Click += new System.EventHandler(this.LabelNaoGosto_Click);
            // 
            // LabelUltimoPost
            // 
            this.LabelUltimoPost.AutoSize = true;
            this.LabelUltimoPost.Location = new System.Drawing.Point(67, 226);
            this.LabelUltimoPost.Name = "LabelUltimoPost";
            this.LabelUltimoPost.Size = new System.Drawing.Size(81, 17);
            this.LabelUltimoPost.TabIndex = 10;
            this.LabelUltimoPost.Text = "ULTIMO POST";
            this.LabelUltimoPost.UseCompatibleTextRendering = true;
            // 
            // LabelData
            // 
            this.LabelData.AutoSize = true;
            this.LabelData.Location = new System.Drawing.Point(194, 226);
            this.LabelData.Name = "LabelData";
            this.LabelData.Size = new System.Drawing.Size(45, 13);
            this.LabelData.TabIndex = 11;
            this.LabelData.Text = "Data: - -";
            this.LabelData.Click += new System.EventHandler(this.LabelData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 494);
            this.Controls.Add(this.LabelData);
            this.Controls.Add(this.LabelUltimoPost);
            this.Controls.Add(this.LabelNaoGosto);
            this.Controls.Add(this.LabelMensagemEnviada);
            this.Controls.Add(this.LabelGosto);
            this.Controls.Add(this.ButtonNaoGosto);
            this.Controls.Add(this.ButtonGosto);
            this.Controls.Add(this.ButtonEnviar);
            this.Controls.Add(this.TextDescricao);
            this.Controls.Add(this.TextTitulo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextTitulo;
        private System.Windows.Forms.TextBox TextDescricao;
        private System.Windows.Forms.Button ButtonEnviar;
        private System.Windows.Forms.Button ButtonGosto;
        private System.Windows.Forms.Button ButtonNaoGosto;
        private System.Windows.Forms.Label LabelGosto;
        private System.Windows.Forms.Label LabelMensagemEnviada;
        private System.Windows.Forms.Label LabelNaoGosto;
        private System.Windows.Forms.Label LabelUltimoPost;
        private System.Windows.Forms.Label LabelData;
    }
}

